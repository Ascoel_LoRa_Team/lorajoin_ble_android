package it.ascoel.lorajoin;

import android.bluetooth.BluetoothProfile;

/**
 * Created by calber on 9/3/17.
 */
public class NewsItem {
    private String reporterName;
    private int rssi;
    private String address;
    private boolean error = false;
    private int status = BluetoothProfile.STATE_DISCONNECTED;
    private boolean transmitted = false;

    public NewsItem(String reporterName, int rssi, String address) {
        this.reporterName = reporterName;
        this.rssi = rssi;
        this.address = address;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isTransmitted() {
        return transmitted;
    }

    public void setTransmitted(boolean transmitted) {
        this.transmitted = transmitted;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReporterName() {
        if(reporterName == null) return "unknown";
        return reporterName;
    }

    public String getAddress() {
        return address;
    }

    public String getRssi() {
        return "RSSI " + rssi + "dBm";
    }

}
