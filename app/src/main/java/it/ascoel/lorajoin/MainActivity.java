package it.ascoel.lorajoin;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.support.design.widget.Snackbar.LENGTH_LONG;


public class MainActivity extends AppCompatActivity {
    private BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 1;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 30000;   // 30 secondi di timeout
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothGatt mGatt;
    private NewsItem selectedItem = null;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    public static final int REQUESTPERMISSION = 11;
    private boolean permissionGrantedCantStart;

    private CoordinatorLayout root;

    static final Map<BluetoothDevice, Integer> scanMap = new HashMap<>();
    BluetoothDevice device;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";


    public String deviceName;
    private ProgressBar connectingProgressBar;
    private ListView devicelist;
    private CustomListAdapter adapter;
    private TextView pintxtbox;
    private Button btnscan;
    private TextInputLayout pinerrorbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        root = (CoordinatorLayout) findViewById(R.id.root);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissionGrantedCantStart = hasPermissionInManifest(this);
        } else {
            permissionGrantedCantStart = true;
        }

    }

    private void setupUi() {
        connectingProgressBar = (ProgressBar) findViewById(R.id.ScanProgressBar);
        connectingProgressBar.setVisibility(View.INVISIBLE);
        pintxtbox = (TextInputEditText) findViewById(R.id.PinTxtBox);
        pinerrorbox = (TextInputLayout) findViewById(R.id.pinlayoutbox);

        pintxtbox.setText(LoraJionApplication.pin);

        findViewById(R.id.info).setOnClickListener(view -> startActivity(new Intent(this, LoadActivity.class)));

        devicelist = (ListView) findViewById(R.id.custom_list);
        adapter = new CustomListAdapter(this, new ArrayList());
        devicelist.setAdapter(adapter);

        devicelist.setOnItemClickListener((a, v, position, id) -> {
            hideKeyboard();
            stopScan();                 // stopScan device scanning

            LoraJionApplication.pin = pintxtbox.getText().toString();
            if("00000".equals(getValidPin())) {
                pinerrorbox.setError("Il pin deve essere composto da 5 numeri");
                return;
            }
            pinerrorbox.setError(null);

            final NewsItem newsData = (NewsItem) devicelist.getItemAtPosition(position);;
            deviceName = newsData.getReporterName();

            Snackbar.make(root, String.format("Connect to %s", newsData.getReporterName()), Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", view -> {
                        connectingProgressBar.setVisibility(View.VISIBLE);
                        connectToDevice(newsData);
                    }).show();
        });

        // istanzia il manager bluetooth
        mHandler = new Handler();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // pulsante scan
        final Button btnscan = (Button) findViewById(R.id.btnScan);
        btnscan.setOnClickListener(v -> scanLeDevice());

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            filters = new ArrayList<>();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!permissionGrantedCantStart) return;
        setupUi();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(root.getWindowToken(), 0);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnStateChangedEvent(Integer status) {
        selectedItem.setStatus(status);
        adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnDataEvent(NewsItem item) {
        if (item.isError()) {
            Snackbar.make(root, "Transmission error", LENGTH_LONG).show();
        } else {
            Snackbar.make(root, "data transfered succesfully", LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
        connectingProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            stopScan();
        }
    }

    @Override
    protected void onDestroy() {
        if (mGatt != null) {
            mGatt.close();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Bluetooth not enabled.
                    finish();
                    return;
                }
                break;
            case REQUESTPERMISSION:
                if (!permissionGrantedCantStart) return;
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    boolean activescan;

    private void scanLeDevice() {
        btnscan = (Button) findViewById(R.id.btnScan);
        connectingProgressBar.setVisibility(View.INVISIBLE);                                        // quando termina la connessione nascondi la progress bar e
        if (activescan) {
            stopScan();
        } else {
            connectingProgressBar.setVisibility(View.VISIBLE);
            btnscan.setText("Stop scan");
            activescan = true;
            scanMap.clear();                                                    // ad ogni riavvio di scan svuota tutta la precedente mappa
            mHandler.postDelayed(() -> stopScan(), SCAN_PERIOD);
            mLEScanner.startScan(filters, settings, mScanCallback);
        }
    }


    private void stopScan() {
        if (!activescan) return;
        connectingProgressBar.setVisibility(View.INVISIBLE);
        activescan = false;
        btnscan.setText("Scan");
        mHandler.removeCallbacksAndMessages(null);      // cancel pending calls to stopScan
        mLEScanner.stopScan(mScanCallback);
    }

    //  Funzione CALLBACK richiamata quando rilevo un device bluetooth
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            scanMap.put(result.getDevice(), result.getRssi());

            ArrayList<NewsItem> results = new ArrayList<>();

            Iterator<Map.Entry<BluetoothDevice, Integer>> itr = scanMap.entrySet().iterator();
            while (itr.hasNext()) {
                Map.Entry<BluetoothDevice, Integer> scanMap = itr.next();
                results.add(new NewsItem(scanMap.getKey().getName(), scanMap.getValue(), scanMap.getKey().getAddress()));
            }
            adapter.load(results);
        }


        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };


    private BluetoothAdapter.LeScanCallback mLeScanCallback = (device1, rssi, scanRecord) -> runOnUiThread(() -> {
        Log.i("onLeScan", device1.toString());
        //connectToDevice(device);  sabato mattina gio
    });


    // Callback che riporta i cambiamenti di stato connessione

    BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);

            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i("gattCallback", "STATE_CONNECTED");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallback", "STATE_DISCONNECTED");
                    gatt.disconnect();
                    selectedItem.setTransmitted(false);
                    break;
                default:
                    Log.e("gattCallback", "STATE_OTHER");
            }
            EventBus.getDefault().post(newState);
        }


        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            UUID CABINET_UUID = UUID.fromString("2999c56e-f707-4140-b368-2e9af0b87ed7");        // CABINET ASCOEL

            //UUID CABINET_UUID = UUID.fromString("000efb1a-9b0d-11e4-89d3-123b93f75cba");
            //UUID CABINET_UUID = UUID.fromString("00002a00-0000-1000-8000-00805f9b34fb");  // basetta ST BlueNRG-1

            Log.i("GIO", "Services Discovered: " + status);
            if (status == GATT_SUCCESS) {
                Log.i("GIO", "No of services discovered: " + gatt.getServices().size());
                mHandler.sendMessage(Message.obtain(null, 201, "No of services discovered: " + gatt.getServices().size()));

                List<BluetoothGattService> services = gatt.getServices();
                for (BluetoothGattService bluetoothGattService : services) {
                    UUID uuid = bluetoothGattService.getUuid();
                    Log.e("GIO", "" + uuid.toString());
                    List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();

                    for (BluetoothGattCharacteristic bluetoothGattCharacteristic : characteristics) {
                        UUID uuidC = bluetoothGattCharacteristic.getUuid();

                        int a = uuidC.compareTo(CABINET_UUID);      // ritorna 0 se il match è soddisfatto
                        Log.i("GIO", "A= : " + String.valueOf(a) + "  UUIDC= :" + uuidC.toString());
                        if (a == 0) {
                            //clientIf, address, handle, writeType, authReq, value
                            //StringBuilder data = new StringBuilder();
                            //data.append("0123456789ABCDEF");//IMEINumber);
                            //data.append("b");
                            //byte[] dataByte = data.toString().getBytes();
                            //byte[] dataByte = data.toString().getBytes();
                            //bluetoothGattCharacteristic.setValue(dataByte);
                            //bluetoothGattCharacteristic.setValue(MyCompleteIdentifier);

                            byte[] dataByte = BuildMyIdentification();
                            bluetoothGattCharacteristic.setValue(dataByte);
                            gatt.writeCharacteristic(bluetoothGattCharacteristic);

/*
                            int number = 2651;
                            String intString = Integer.toString(number);
                            byte b = Byte.parseByte(intString, 16);
                            */
                            //Log.i("GIO", b);

/*
                            StringBuilder data1 = new StringBuilder();
                            data1.append("abcd");
                            byte[] dataByte1 = data1.toString().getBytes();
                            bluetoothGattCharacteristic.setValue(dataByte1);
                            gatt.writeCharacteristic(bluetoothGattCharacteristic);
*/
                            Log.i("GIO", "SCRITTO QUALCHE MINCHIATA");

                        }

                        Log.i("GIO", "Gatt Properties : " + bluetoothGattCharacteristic.getProperties());
                        Log.i("GIO", "" + uuidC.toString());
                        CharacteristicHelper helper = new CharacteristicHelper(bluetoothGattCharacteristic);
                        Log.i("GIO", "isRead : " + helper.isRead());
                        Log.i("GIO", "isWrite : " + helper.isWrite());
                        Log.i("GIO", "isNotify : " + helper.isNotify());
                        Log.i("GIO", "isWriteNoResponse : " + helper.isWriteNoResponse());

                    }
                }
            }
            /*List<BluetoothGattService> services = gatt.getServices();
            Log.i("onServicesDiscovered", services.toString());
            gatt.readCharacteristic(services.get(1).getCharacteristics().get(0));*/
        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i("GIO", characteristic.toString());
            gatt.disconnect();
        }


        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic, int status) {

            Log.i("GIO", "writeCharacteristic111");
            Log.i("GIO", "status:" + status);
            //Log.i("GIO", GATT_SUCCESS);
            if (status == GATT_SUCCESS) {
                //broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                Log.i("GIO", "GATT_SUCCESS");
                selectedItem.setTransmitted(true);
                EventBus.getDefault().post(selectedItem);
            } else {
                Log.w("GIO", "FAILED");
                selectedItem.setError(true);
                EventBus.getDefault().post(selectedItem);
            }
        }
    };


    public boolean connectToDevice(NewsItem data) {
        final String address = data.getAddress();
        if (mBluetoothAdapter == null || address == null) {
            Log.w("GIO", "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w("GIO", "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect parameter to false.

        selectedItem = data;
        mGatt = device.connectGatt(this, false, gattCallback);
        Log.d("GIO", "Trying to create a new connection.");
        return true;
    }


    // Filtering by custom UUID is broken in Android 4.3 and 4.4, see:
    // http://stackoverflow.com/questions/18019161/startlescan-with-128-bit-uuids-doesnt-work-on-native-android-ble-implementation?noredirect=1#comment27879874_18019161
    // This is a workaround function from the SO thread to manually parse advertisement data.
    private List<UUID> parseUuids(byte[] advertisedData) {
        List<UUID> uuids = new ArrayList<>();

        ByteBuffer buffer = ByteBuffer.wrap(advertisedData).order(ByteOrder.LITTLE_ENDIAN);
        while (buffer.remaining() > 2) {
            byte length = buffer.get();
            if (length == 0) break;

            byte type = buffer.get();
            switch (type) {
                case 0x02: // Partial list of 16-bit UUIDs
                case 0x03: // Complete list of 16-bit UUIDs
                    while (length >= 2) {
                        uuids.add(UUID.fromString(String.format("%08x-0000-1000-8000-00805f9b34fb", buffer.getShort())));
                        length -= 2;
                    }
                    break;

                case 0x06: // Partial list of 128-bit UUIDs
                case 0x07: // Complete list of 128-bit UUIDs
                    while (length >= 16) {
                        long lsb = buffer.getLong();
                        long msb = buffer.getLong();
                        uuids.add(new UUID(msb, lsb));
                        length -= 16;
                    }
                    break;

                default:
                    buffer.position(buffer.position() + length - 1);
                    break;
            }
        }

        return uuids;
    }

    //************************************************************************************************************************************************
    // costruisci il pacchetto con imei e pin compressi in 10 bytes prima di trasmetterlo al BLE nel cabinet

    public byte[] BuildMyIdentification() {
        String Pin, IMEINumber;
        byte[] MyCompleteIdentifier = new byte[10];

        //Get IMEI phone
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        IMEINumber = tm.getDeviceId();

        // Get Pin
        Pin = getValidPin();

        IMEINumber = IMEINumber + Pin;                                                              // concatena imei e pin
        List<String> list = new ArrayList<>();
        int index = 0;                                                                              // trasforma le 20 cifre da 0 a 9 (15 imei + 5 pin)
        while (index < IMEINumber.length()) {                                                       // in 10 bytes. Ogni cifra occuperà un nibble
            list.add(IMEINumber.substring(index, Math.min(index + 1, IMEINumber.length())));          // del byte destinazione.
            index++;
        }
        int h, l;
        int i = 0;
        for (int y = 0; y < 20; y += 2) {
            h = Integer.parseInt(list.get(y));
            l = Integer.parseInt(list.get(y + 1));
            MyCompleteIdentifier[i] = (byte) (h * 16 + l);
            i++;
        }
        return MyCompleteIdentifier;
    }

    @NonNull
    private String getValidPin() {
        String Pin;
        Pin = pintxtbox.getText().toString();                                                       // una lunghezza di 5 cifre (obbligo)
        Pin = Pin.trim();                                                                           // elimina gli spazi  e se entry è invalido
        if ((Pin.length() == 0) || (Pin.length() < 5))                                                  // setta il pin a 00000
        {
            // Toast.makeText(getApplicationContext(), "Invalid Pin.", Toast.LENGTH_SHORT).show();
            Pin = "00000";
        } else {
            String regexStr = "^[0-9]*$";                                                           // RegExxp per validare solo numeri
            Pin = Pin.substring(0, 5);
            if (Pin.matches(regexStr))                                                               // verifica se solo numerico
            {
                // Toast.makeText(getApplicationContext(), Pin, Toast.LENGTH_SHORT).show();
            } else {
                //  Toast.makeText(getApplicationContext(), "Regex Invalid Pin.", Toast.LENGTH_SHORT).show();
                Pin = "00000";
                pintxtbox.setText("");
            }
        }
        return Pin;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean flag = true;
        for (int c : grantResults) {
            if (c < 0) flag = false;
        }
        permissionGrantedCantStart = flag;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermissionInManifest(Context context) {
        boolean flag = true;

        List<String> deniedPermission = new ArrayList<>();
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final List<String> declaredPermisisons = Arrays.asList(packageInfo.requestedPermissions);
            for (String p : declaredPermisisons) {
                if (checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED) {
                    try {
                        shouldShowRequestPermissionRationale(p);
                    } catch (Exception e) {
                        // if we end up here permission is custom (i.e. onesignal use them) and is not relevant
                        continue;
                    }
                    deniedPermission.add(p);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
        }

        if (!deniedPermission.isEmpty()) {
            requestPermissions(deniedPermission.toArray(new String[deniedPermission.size()]), REQUESTPERMISSION);
            flag = false;
        }

        return flag;
    }

}
