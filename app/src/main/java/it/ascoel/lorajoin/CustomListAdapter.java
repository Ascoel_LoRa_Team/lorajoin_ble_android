package it.ascoel.lorajoin;


import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter {
    private ArrayList<NewsItem> listData;
    private LayoutInflater layoutInflater;


    public CustomListAdapter(Context aContext, ArrayList<NewsItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }


    public void load(ArrayList<NewsItem> listData) {
        this.listData = listData;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        NewsItem newsItem = listData.get(position);

        switch (newsItem.getStatus()) {
            case BluetoothProfile.STATE_DISCONNECTED:
                holder.status.setVisibility(View.INVISIBLE);
                break;
            case BluetoothProfile.STATE_CONNECTED:
                holder.status.setVisibility(View.VISIBLE);
                break;
            default:
                holder.status.setVisibility(View.INVISIBLE);
                break;
        }
        if(newsItem.isTransmitted()) {
            holder.box.setBackgroundResource(R.color.colorAccent);
        } else {
            holder.box.setBackground(null);
        }

        holder.headlineView.setText(newsItem.getReporterName());
        holder.reporterNameView.setText(newsItem.getAddress());
        holder.reportedDateView.setText(newsItem.getRssi());
        return convertView;
    }

    static class ViewHolder {
        TextView headlineView;
        TextView reporterNameView;
        TextView reportedDateView;
        ImageView status;
        View box;

        public ViewHolder(View convertView) {
            headlineView = (TextView) convertView.findViewById(R.id.title);
            reporterNameView = (TextView) convertView.findViewById(R.id.reporter);
            reportedDateView = (TextView) convertView.findViewById(R.id.rssi);
            status = (ImageView) convertView.findViewById(R.id.status);
            box= convertView.findViewById(R.id.box);
        }
    }
}
